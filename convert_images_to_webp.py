from pathlib import Path
from PIL import Image

def convert_to_webp(source):
    """Convert image to WebP.

    Args:
        source (pathlib.Path): Path to source image

    Returns:
        pathlib.Path: path to new image
    """
    destination = source.with_suffix(".webp")

    image = Image.open(source)  # Open image
    image.save(destination, format="webp")  # Convert image to webp

    return destination

def main():
    paths = Path("www/static/images").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/content").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/content").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/icon").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/icon").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/MemberAndPartner").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/MemberAndPartner").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/product").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/product").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/seo").glob("**/*.png")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

    paths = Path("www/static/images/seo").glob("**/*.jpg")
    for path in paths:
        webp_path = convert_to_webp(path)
        print(webp_path)

main()


