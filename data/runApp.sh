#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT


#curl --location --request POST 'http://kong.planblick.svc:8001/services/' \
#--header 'Content-Type: application/json' \
#--data-raw '{
#  "name": "crowdsoft-website",
#  "url": "http://crowdsoft-website.planblick.svc:80"
#}'
#
#
#curl --location --request POST 'http://kong.planblick.svc:8001/services/crowdsoft-website/routes/' \
#--header 'Content-Type: application/json' \
#--data-raw '{
#  "hosts": ["crowdsoft.net", "www.crowdsoft.net", "stagingwww.crowdsoft.net"]
#}'

nginx