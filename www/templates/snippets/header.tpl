 <!--start header snippet -->
 <div class="container">
            <!-- Logo container-->
            <div>
                <a class="logo" href="index.html">
                    <picture>
                        <source srcset="/static/images/logo.webp" />
                        <img src="/static/images/logo.png" alt="Logo" height="24">
                    </picture>
                </a>
            </div>
            <!-- End Logo container-->

            <div class="menu-extras">
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a href="#" class="navbar-toggle" id="isToggle" onclick="toggleMenu();return false;" aria-label="Mobile Menue Toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

            <!--Login button Start-->
            <ul class="buy-button list-inline mb-0">
                <li class="list-inline-item button mb-0 ps-1">
                    <a href="https://gitlab.com/crowdsoft-foundation" target="_blank" class="btn btn-icon btn-primary" aria-label="Gitlab Crowdsoft"><i data-feather="gitlab"
                            class="fea icon-sm fea-social"></i></a>
                </li>
<!--                <li class="list-inline-item button mb-0 ps-1">-->
<!--                    <a href="javascript:void(0)" class="btn btn-icon btn-primary" data-bs-toggle="modal"-->
<!--                        data-bs-target="#loginform"><i data-feather="user" class="fea icon-sm fea-social"></i></a>-->
<!--                </li>-->
            </ul>
            <!--Login button End-->

            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu nav-dark">
                    <li class="has-submenu parent-menu-item">
                        <a href="#" data-toggle="expand">Stiftungs Idee</a><span class="menu-arrow"
                            data-toggle="expand"></span>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="index.html#mission" class="sub-menu-item">Mission</a>
                                        <p class="sub-menu__text text-wrap">Zweck der Stiftung, ihre Werte, Vision und
                                            Ziele</p>
                                    </li>
                                    <li><a href="index.html#organisation" class="sub-menu-item">Organisation</a>
                                        <p class="sub-menu__text text-wrap">Die Struktur der Stiftung, ihre Begünstigten
                                            und Beteiligten</p>
                                    </li>
                                    <li><a href="index.html#foerderungen" class="sub-menu-item">Förderungen</a>
                                        <p class="sub-menu__text text-wrap">Formen der Unterstützung und Förderungen im
                                            Sinne des Stiftungszwecks</p>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="has-submenu parent-menu-item">
                        <a href="#" data-toggle="expand">Software</a><span class="menu-arrow"
                            data-toggle="expand"></span>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><span class="sub-menu__title">Open Source Framework
                                        </span></li>
                                    <li><a href="Web-Application-Stack-Platform.html" class="sub-menu-item">Web-Application-Stack Platform
                                            </a>
                                        <p class="sub-menu__text text-wrap">Entwicklungsinfrastruktur als Basis aller
                                            Anwendungen</p>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><span class="sub-menu__title">Out-of-the-Box
                                        </span></li>
                                    <li><a href="./software-application.html" class="sub-menu-item">Workplace Suite</a>
                                        <p class="sub-menu__text text-wrap">Anwendungen, die das Alltagsgeschäft
                                            vereinfachen
                                        </p>
                                    </li>

                                </ul>

                            </li>
                        </ul>
                    </li>
                    <li class="has-submenu parent-menu-item">
                        <a href="#" data-toggle="expand"> Community</a> <span class="menu-arrow"
                            data-toggle="expand"></span>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="https://forum.crowdsoft.net" aria-label="Crowdsoft Forum"
                                            target="_blank" rel="noopener" class="sub-menu-item">Forum</a>
                                        <p class="sub-menu__text text-wrap">Hilfe und Diskussionen mit
                                            Community-Mitgliedern</p>
                                    </li>
                                    <li><a href="https://gitlab.com/crowdsoft-foundation"
                                            aria-label="Crowdsoft Gitlab Repository" target="_blank" rel="noopener"
                                            class="sub-menu-item">Kollaboration</a>
                                        <p class="sub-menu__text text-wrap">Probleme melden, Pull Requests erstellen</p>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--end navigation menu-->
                <div class="buy-menu-btn d-none">
                    <!-- buttons mobile view -->
                    <li class="list-inline-item button mb-0 ps-1">
                        <a href="https://gitlab.com/crowdsoft-foundation" class="btn btn-icon btn-soft-primary"><i data-feather="gitlab"
                                class="fea icon-sm fea-social"></i></a>
                    </li>
                    <!--li class="list-inline-item button mb-0 ps-1">
                        <a href="javascript:void(0)" class="btn btn-icon btn-soft-primary" data-bs-toggle="modal"
                            data-bs-target="#loginform"><i data-feather="user" class="fea icon-sm fea-social"></i></a>
                    </li-->
                </div>
                <!--end buttons mobile view-->
            </div>
            <!--end navigation-->
        </div>
        <!--end container-->
 <!--end header snippet -->