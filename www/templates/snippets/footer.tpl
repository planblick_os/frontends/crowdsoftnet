<!--start footer snippet -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer-py-60">
                        <div class="row">
                            <div class="col-lg-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                                <h3 class="text-light footer-head">Folge uns
                                </h3>
                                <p class="mt-4">Über unsere Aktivität auf den Sozialen Netzwerken erhältst Du
                                    Neuigkeiten und Aktionen über Crowdsoft. Neben Informationsweitergabe ist
                                    dies unsere Möglichkeit der Vernetzung zwischen unseren Nutzern, Unterstützern
                                    und Entwicklern.
                                </p>
                                <ul class="list-unstyled social-icon social mb-0 mt-4">
<!--                                    <li class="list-inline-item"><a href="https://www.facebook.com/"-->
<!--                                            target="_blank" class="rounded"><i data-feather="facebook"-->
<!--                                                class="fea icon-sm fea-social"></i></a></li>-->
                                    <li class="list-inline-item"><a href="https://gitlab.com/crowdsoft-foundation"
                                            target="_blank" class="rounded" aria-label="Gitlab Crowdsoft-Foundation"><i data-feather="gitlab"
                                                class="fea icon-sm fea-social"></i></a></li>
<!--                                    <li class="list-inline-item"><a-->
<!--                                            href="https://www.linkedin.com/company/" target="_blank"-->
<!--                                            class="rounded"><i data-feather="linkedin"-->
<!--                                                class="fea icon-sm fea-social"></i></a></li>-->
<!--                                    <li class="list-inline-item"><a href="https://www.xing.com/companies/"-->
<!--                                            target="_blank" class="rounded"><img src="/static/images/icon/xing-64-light.png"-->
<!--                                                alt="xing" class="fea icon-sm fea-social"></a></li>-->
                                </ul>
                                <!--end icon-->
                            </div>
                            <!--end col-->

                            <div class="col-lg-2 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <h3 class="text-light footer-head">Unterstützer</h3>
                                <ul class="list-unstyled footer-list mt-4">
                                    <li><a href="https://www.planblick.com/" target="_blank" class="text-foot"><span
                                                class="material-icons-outline align-middle me-1">chevron_right</span>
                                            About planBLICK</a></li>
<!--
                                    <li><a href="https://crowdsoft.net/login/" target="_blank" class="text-foot"><span
                                                class="material-icons-outline align-middle me-1">chevron_right</span>
                                            Login</a></li>
-->
                                </ul>
                                </ul>
                            </div>
                            <!--end col-->

                            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                                <h3 class="text-light footer-head">Legal</h3>
                                <ul class="list-unstyled footer-list mt-4">
                                    <li><a href="https://impressum.planblick.com" target="_blank"
                                            class="text-foot"><span
                                                class="material-icons-outline align-middle me-1">chevron_right</span>
                                            Impressum</a></li>
                                    <li><a href="https://datenschutz.planblick.com" target="_blank"
                                            class="text-foot"><span
                                                class="material-icons-outline align-middle me-1">chevron_right</span>
                                            Datenschutz</a></li>
                                </ul>
                            </div>
                            <!--end col-->

                            <div class="col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0" style="display: none;">
                                <h3 class="text-light footer-head">Bleib auf dem Laufenden!</h3>
                                <p class="mt-4">Innovation geschieht kontinuierlich bei Crowdsoft. Möchtest Du einen
                                    kleinen Einblick erhalten, woran wir arbeiten? Melde Dich noch heute an, um immer informiert zu sein.
                                </p>
                                <p id="subscribe-error-msg"></p>
                                <form id="newsletter_form" method="post" name="myNewsletterForm"
                                    enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="foot-subscribe mb-3">
                                                <label class="form-label">E-Mail eingeben <span
                                                        class="text-danger">*</span></label>
                                                <div class="form-icon position-relative">
                                                    <i data-feather="mail" class="fea icon-sm icons"></i>
                                                    <input type="email" name="email-subscribe" id="emailsubscribe"
                                                        class="form-control ps-5 rounded" placeholder="E-mail: ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="d-grid">
                                                <button type="submit" id="submitsubscribe" name="subscribe"
                                                    class="btn btn-soft-primary">Anmelden</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->

        <div class="footer-py-30 footer-bar">
            <div class="container text-center">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="text-sm-start">
                            <p class="mb-0">©
                                2022 Crowdsoft Foundation
                            </p>
                        </div>
                    </div>
                    <!--end col-->

                    <div class="col-sm-6 mt-4 mt-sm-0 pt-2 pt-sm-0">


                    </div>
                    <!--end col-->
                </div>
                <!--end row-->
            </div>
            <!--end container-->
        </div>
    </footer>

    <!-- Back to top -->
    <a href="#" onclick="topFunction();return false;" id="back-to-top" class="back-to-top fs-5"><i data-feather="arrow-up" class="fea icon-sm icons align-middle"></i></a>
    <!-- Back to top -->
    
<!--end footer snippet -->