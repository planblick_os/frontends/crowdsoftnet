
// Contact form
function validateProjectForm(event) {
    //alert("am validating")
    event.preventDefault()

    var email = document.forms["myProjectForm"]["email"].value;
    var name = document.forms["myProjectForm"]["name"].value;
    document.getElementById("error-msg").style.opacity = 0;
    document.getElementById('error-msg').innerHTML = "";

    if (name == "" || name == null) {
        document.getElementById('error-msg').innerHTML = "<div class='alert alert-warning error_message'>*Bitte geben Sie Ihre Name ein.*</div>";
        fadeIn();
        return false;

    }

    if (email == "" || email == null) {
        document.getElementById('error-msg').innerHTML = "<div class='alert alert-warning error_message'>*Bitte geben Sie eine E-Mail-Adresse ein.*</div>";
        fadeIn();
        return false;

    }

    sendContactMail(email)
    return false;

}

// show and focus error message
function fadeIn() {
    var fade = document.getElementById("subscribe-error-msg");
    var opacity = 0;
    var intervalID = setInterval(function () {
        if (opacity < 1) {
            opacity = opacity + 0.5
            fade.style.opacity = opacity;

            //  $([document.documentElement, document.body]).animate({
            //    scrollTop: $("#form_top").offset().top
            //}, 200);

        } else {
            clearInterval(intervalID);
        }
    }, 200);
}

function sendContactMail() {

    //alert("calling form script")

    let name = $("#nameProjectForm").val()
    let email = $("#emailProjectForm").val()

    let checkedInterests = ""
    for (var i = 0; i < document.myProjectForm.interest.length; i++) {
        if (document.myProjectForm.interest[i].checked)
            checkedInterests += document.myProjectForm.interest[i].value + " , "
    }
 
    let message = "Wie können wir sie unterstützen: " + checkedInterests + "\n Mehr über ihr Projekt erzählen : " + $("#comments").val() + "\n Welche Unterstützung wünschen sie: " + $("#comments_wish").val()

   // alert(message)

    if (!name) {
        alert("Name ist ein Pflichtfeld")
        return false
    }

    if (!email) {
        alert("Email ist ein Pflichtfeld")
        return false
    }

    let settings = {
        //call the test contact api to test for error message
        "url": "https://api.planblick.com/contact_mail",
        //"url": "https://api.test.com/contact_mail",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        //change the product for what ever the form context is, and set others as variable or string
        "data": JSON.stringify({
            "product": "planblick-Website Projekt Starten",
            "name": name,
            "email": email,
            "message": message
        }),
        "success": function () {
            location.replace("./kontakt-confirm.html")
        },
        "error": function () {
            alert("Ihre Nachricht konnte nicht zugestellt werden, bitte versuchen Sie es erneut oder schreiben Sie uns eine Email an: \ninfo@planblick.com")
        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
}

