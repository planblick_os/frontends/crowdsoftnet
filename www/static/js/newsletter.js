
document.getElementById("newsletter_form").addEventListener("submit", function(event){
    validateForm(event)
  })
  
  // Newsletter
  function validateNewsletterForm(event) {
    event.preventDefault()
  
    var email = document.forms["myNewsletterForm"]["emailsubscribe"].value;
    document.getElementById("subscribe-error-msg").style.opacity = 0;
    document.getElementById('subscribe-error-msg').innerHTML = "";
  
    if (email == "" || email == null) {
      document.getElementById('subscribe-error-msg').innerHTML = "<div class='alert alert-warning error_message'>*Bitte geben Sie eine E-Mail-Adresse ein.*</div>";
      fadeInNewsletterError();
      return false;

    }

    sendNewsletterMail(email)
    return false;
  
  }
  
  // show and focus error message
  function fadeInNewsletterError() {
    var fade = document.getElementById("subscribe-error-msg");
    var opacity = 0;
    var intervalID = setInterval(function () {
      if (opacity < 1) {
        opacity = opacity + 0.5
        fade.style.opacity = opacity;
        
      //  $([document.documentElement, document.body]).animate({
      //    scrollTop: $("#form_top").offset().top
      //}, 200);
  
      } else {
        clearInterval(intervalID);
      }
    }, 200);
  }
  
  function sendNewsletterMail(email) {
  let settings = {
          //call the test contact api to test for error message
          "url": "https://api.planblick.com/contact_mail",
          //"url": "https://api.test.com/contact_mail",
          "method": "POST",
          "timeout": 0,
          "headers": {
          "Content-Type": "application/json"
          },
          //change the product for what ever the form context is, and set others as variable or string
          "data": JSON.stringify({
              "product": "planblick-Newsletter anmelden",
              "name": "Kein Name angegeben",
              "email": email,
              "message": "Ja, ich möchte den Newsletter erhalten."
          }),
          "success": function () {
              //location.replace("./kontakt-confirm.html")
              document.getElementById('subscribe-error-msg').innerHTML = "<div class='alert alert-warning success_message'>Danke, Sie sind für den Newsletter registriert.</div>";
              fadeInNewsletterError();

          },
          "error": function () {
              
              document.getElementById('subscribe-error-msg').innerHTML = "<div class='alert alert-warning error_message'>Ihre Nachricht konnte nicht zugestellt werden, bitte versuchen Sie es erneut oder schreiben Sie uns eine Email an: info@planblick.com</div>";
              fadeInNewsletterError();
         
          },
      };
  
      $.ajax(settings).done(function (response) {
          console.log(response);
      });
    }