export class Map {
    center = [48.1196598, 11.5320500]
    zoom = 11
    mapOptions = {
        center: Map.center,
        zoom: Map.zoom,
    }
    map = undefined
    iconOptions = {
        title: '',
        draggable: false,
    }
    markers = []
    searchMarker = undefined
    bounds = undefined



    constructor() {
        if (!Map.instance) {
            Map.instance = this
        }
        return Map.instance
    }

    initMap(markers=undefined) {
        Map.instance.map = L.map('map').setView(Map.instance.center, Map.instance.zoom);
        Map.instance.map.on('geosearch/showlocation', () => {
            Map.instance.onsearch()
            return false
          });

        let layer = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 50,
            // All following options are default settings
            minZoom: 0,
            errorTileUrl: '',
            zoomReverse: false,
            detectRetina: false,
            crossOrigin: false,
            updateWhenZooming: true,
            updateInterval: 200,
            zIndex: 1,
            bounds: undefined,
            maxNativeZoom: undefined,
            minNativeZoom: undefined,
            noWrap: false,
            className: '',
            keepBuffer: 2
        })
        Map.instance.map.addLayer(layer)


        if(markers) {
            for(let marker of markers) {
                Map.instance.marker = new L.Marker([marker.lat, marker.long], {
                    title: marker.title,
                    draggable: false
                })

                // create popup contents
                let customPopup = `${marker.title}<br/><a href=${marker.url} target="_blank">Website</a>`

                // specify popup options
                let customOptions = {
                    'maxWidth': '500',
                    'className' : 'custom'
                }
                console.log("Custom Popup", marker.url)
                Map.instance.marker.addTo(Map.instance.map)
                Map.instance.marker.bindPopup(customPopup, customOptions).openPopup()
                Map.instance.markers.push(Map.instance.marker)
            }
            Map.instance.setBounds()
            }


        Map.instance.searchMarker = new L.Marker([0, 0], Map.instance.iconOptions)
        let searchControl = new window.GeoSearch.GeoSearchControl({
            provider: new window.GeoSearch.OpenStreetMapProvider({
                showMarker: true,
                marker: Map.instance.searchMarker, // use custom marker, not working?
            }),
        });
        return Map.instance
    }

    search(value) {
        return new Promise(function (resolve, reject) {
            const provider = new window.GeoSearch.OpenStreetMapProvider();
            provider.search({ query: value }).then((data)=> {
                if(data[0]) {
                    Map.instance.setSearchPoint(data[0]["y"], data[0]["x"])
                }
                resolve(data)
            })
        })
    }

    setSearchPoint(y, x) {
        if(Map.instance.searchMarker instanceof L.Marker) {
            Map.instance.searchMarker.removeFrom(Map.instance.map)
        }
        Map.instance.searchMarker = new L.Marker([y, x], Map.instance.iconOptions)
        Map.instance.searchMarker.addTo(Map.instance.map)
        Map.instance.map.panTo(Map.instance.searchMarker._latlng)
        Map.instance.setBounds()

    }

    getDistanceFromLatLonInKm(position1, position2) {
        var deg2rad = function (deg) { return deg * (Math.PI / 180); },
            R = 6371,
            dLat = deg2rad(position2.lat - position1.lat),
            dLng = deg2rad(position2.lng - position1.lng),
            a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(deg2rad(position1.lat))
                * Math.cos(deg2rad(position2.lat))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2),
            c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    getDistancesToPoint(y, x) {
        let return_value = []
        for(let marker of Map.instance.markers) {
            let distance = Map.instance.getDistanceFromLatLonInKm(marker.getLatLng(), Map.instance.searchMarker.getLatLng())
            return_value.push({"label": marker.getPopup().getContent(), "distance": distance})
        }
        return_value.sort((a,b) => a.distance - b.distance)

        return return_value
    }
    setBounds () {
        let bounds = new L.LatLngBounds();
        Map.instance.map.eachLayer(item => {
            if (item instanceof L.Marker) {
                bounds.extend(item.getLatLng());
            }
        });
        Map.instance.bounds = bounds
        Map.instance.map.fitBounds(Map.instance.bounds,{"padding": [10, 10]})
    }
    setNewBounds(label){
        if(Map.instance.markers){
            const newCenter = Map.instance.markers.find(element => element.options.title == label)
            if(Map.instance.searchMarker){
                newCenter.bindPopup(label).openPopup()
                Map.instance.map.panTo(Map.instance.searchMarker._latlng)
                Map.instance.map.fitBounds([Map.instance.searchMarker._latlng, newCenter._latlng], {"padding": [10, 10]})
            }
            else{
                newCenter.bindPopup(label).openPopup()
                Map.instance.map.panTo(Map.instance.searchMarker._latlng)
                Map.instance.map.fitBounds(Map.instance.bounds,{"padding": [10, 10]})
                map.setZoom(16)
            }

        }
    }
    // use if no Searchmarker is set, initially for top Entry of list
    setCenter(label) {
        console.log("Work",label)
        if(Map.instance.markers){
            const newCenter = Map.instance.markers.find(element => element.options.title == label)
            console.log(Map.instance.markers)
            newCenter.bindPopup(label).openPopup()
            Map.instance.map.panTo(Map.instance.newCenter._latlng)
        }
    }
    


}



