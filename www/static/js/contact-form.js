
// Contact form
function validateContactForm(event) {
    // alert("am validating")
    event.preventDefault()

    var email = document.forms["myContactForm"]["email"].value;
    var name = document.forms["myContactForm"]["name"].value;
    document.getElementById("error-msg").style.opacity = 0;
    document.getElementById('error-msg').innerHTML = "";

    if (name == "" || name == null) {
        document.getElementById('error-msg').innerHTML = "<div class='alert alert-warning error_message'>*Bitte geben Sie Ihre Name ein.*</div>";
        fadeIn();
        return false;

    }

    if (email == "" || email == null) {
        document.getElementById('error-msg').innerHTML = "<div class='alert alert-warning error_message'>*Bitte geben Sie eine E-Mail-Adresse ein.*</div>";
        fadeIn();
        return false;

    }

    sendContactMail(email)
    return false;

}

// show and focus error message
function fadeIn() {
    var fade = document.getElementById("error-msg");
    var opacity = 0;
    var intervalID = setInterval(function () {
        if (opacity < 1) {
            opacity = opacity + 0.5
            fade.style.opacity = opacity

        } else {
            clearInterval(intervalID);
        }
    }, 200);
}

function sendContactMail() {


    let name = $("#nameInput").val()
    let email = $("#emailInput").val()
    let message = " Art der Anfrage: " + $("#topicSelect option:selected").val() + "\n Nachricht: " + $("#messageInput").val()

    //console.log(name, email, message)

    if (!name) {
        alert("Name ist ein Pflichtfeld")
        return false
    }

    if (!email) {
        alert("Email ist ein Pflichtfeld")
        return false
    }

    let settings = {
        //call the test contact api to test for error message
        "url": "https://api.planblick.com/contact_mail",
        //"url": "https://api.test.com/contact_mail",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        //change the product for what ever the form context is, and set others as variable or string
        "data": JSON.stringify({
            "product": "Crowdsoft-Website Kontaktform",
            "name": name,
            "email": email,
            "message": message
        }),
        "success": function () {
            document.getElementById('error-msg').innerHTML = "<div class='alert alert-success success_message'>Wir haben Ihre Nachricht erhalten und möchten uns bei Ihnen für Ihr Schreiben bedanken. Wir werden so schnell wie möglich per E-Mail antworten</div>";
            fadeIn();
        },
        "error": function () {
            document.getElementById('error-msg').innerHTML = "<div class='alert alert-danger success_message'>Ihre Nachricht konnte nicht zugestellt werden, bitte versuchen Sie es erneut oder schreiben Sie uns eine Email an: \ninfo@planblick.com</div>";
            fadeIn();

        },
    };

    $.ajax(settings).done(function (response) {
        console.log(response);
    });


}

