import {Map} from "./map.js";
let map = new Map(document.getElementById("search"))
let dealers = []
fetchDealerData().then((data) => {
    map.initMap(data)
    renderList(data)
    dealers = data
})



document.getElementById("searchButton").addEventListener("click", function() {
    var search = document.getElementById("searchInput").value;
    document.getElementById("results").innerHTML = "";
    searchMap(search)
})

document.getElementById("cancelButton").addEventListener("click", function() {
    document.getElementById("searchInput").value= "";
    document.getElementById("results").innerHTML = "";
})

document.getElementById("searchInput").addEventListener("input", function() {
    var search = document.getElementById("searchInput").value;
    document.getElementById("results").innerHTML = "";
    searchMap(search)
})

const renderList = (data, updated=false) => {
    $("#scrollable-list-content").html("")
    let listEntry = $("#entry-card-template").html()
    for(let entry of data) {
        let newEntry
        let fieldContent = listEntry.replace("[{name}]", entry.title).replace("[{url}]", entry.url).replace("[{linklabel}]", entry.linklabel).replace("[{distance}]", entry.distance ? `${entry.distance} km` : "" ).replace("[{id}]", entry.title)
        newEntry = fieldContent
        $("#scrollable-list-content").append(newEntry)
    }
    if (!updated){
        let list = document.getElementsByClassName("partner-list-entry")
        Array.from(list).forEach(entry => {
                    entry.addEventListener("click", event => {
                       console.log("Clickhandler Label",entry.id)
                       map.setNewBounds(entry.id)
                    })
                })

    }
}

const searchMap = (searchInput) => {
    map.search(searchInput).then((data) => {
        for (let i = 0; i < data.length; i++) {
            let para = document.createElement("p");
            para.innerText = data[i].label;
            para.addEventListener("click", ()=> {
                map.setSearchPoint(data[i].y, data[i].x)
                document.getElementById("results").innerHTML = "";
                let distances = map.getDistancesToPoint(data[i].y, data[i].x)
                let tmp_data = dealers
                distances.forEach(distance => {
                    tmp_data.map(entry =>{
                        if (distance.label == entry.title){
                           entry.distance = Math.round(distance.distance)
                        }
                    } )
                });

                tmp_data.sort((a,b) => a.distance - b.distance)
                renderList(tmp_data, true)
                let updatedList = document.getElementsByClassName("partner-list-entry")
                Array.from(updatedList).forEach(entry => {
                    entry.addEventListener("click", event => {
                       map.setNewBounds(entry.id)
                    })
                })
            })
            document.getElementById("results").appendChild(para);
        }
    })
}

document.addEventListener("keyup", function(event) {
      let enterKeyCode = 13
      if (event.keyCode === enterKeyCode && document.getElementById("searchInput").value != "") {
            event.preventDefault();
            document.getElementById("searchButton").click();
        }
})


function fetchDealerData() {
    return new Promise((resolve, reject) => {
        let dealers = [
            {
                "id": 1,
                "lat": 48.1196598,
                "long": 11.5320500,
                "title": "planBLICK GmbH",
                "url": "https://planblick.com/cloud-anwendungen.html",
                "linklabel": "www.planblick.com"
            }
        ]
        resolve(dealers)
    })
}
